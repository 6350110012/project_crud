import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:upload_image1/Insert.dart';

import 'Products.dart';
import 'Team.dart';
import 'details.dart';
import 'Insert.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> {
  late Future<List<Products>> products;

  @override
  void initState() {
    super.initState();
    products = getProductsList();
  }

  Future<List<Products>> getProductsList() async {
    String url = "http://10.0.2.2/android/list_products.php";
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return productsFromJson(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load products');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Bakery List'),
        backgroundColor: Colors.brown,

      ),
      body: Center(
        child: FutureBuilder<List<Products>>(
          future: products,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            // By default, show a loading spinner.
            if (!snapshot.hasData) return CircularProgressIndicator();
            // Render Products lists
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                var data = snapshot.data[index];
                return Card(
                  child: ListTile(
                    leading: Icon(Icons.bakery_dining_rounded,color: Colors.black45),
                    trailing: Icon(Icons.filter_list),
                    subtitle: Text(data.price),


                    title: Text(
                      data.name,

                      style: TextStyle(fontSize: 19,color: Colors.brown),
                    ),

                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Details(products: data)),
                      );
                    },
                  ),
                );
              },
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor:Colors.brown,
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => CreatePro()),
          );
        },
      ),
      drawer:Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            Padding(
              padding: const EdgeInsets.only(top:50,bottom:20 ),
              child: Image.asset('asset/images/โลโก้.png',),
            ),
            Divider(
              thickness: 1.0,
            ),
            ListTile(
              leading: Icon(Icons.bakery_dining_rounded,color:Colors.brown,
              ),
              title: const Text('Bakery List ',style: TextStyle(fontSize: 17,color: Colors.brown)),
              onTap: () {
                //Navigator.pop(context);
                Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => MyApp(),
                    )

                );
              },
            ),
            Divider(
              thickness: 1.0,
            ),

            ListTile(
              leading: Icon(Icons.bakery_dining_rounded,color:Colors.brown,
              ),
              title: const Text('Bakery Insert ',style: TextStyle(fontSize: 17,color: Colors.brown)),
              onTap: () {
                //Navigator.pop(context);
                Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => CreatePro(),
                    )

                );
              },
            ),

            Divider(
              thickness: 1.0,
            ),


            ListTile(
              leading: Icon(Icons.bakery_dining_rounded,color:Colors.brown,
              ),
              title: const Text('Team',style: TextStyle(fontSize: 17,color: Colors.brown)),
              onTap: () {
                //Navigator.pop(context);
                Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => ProfileDeveloper(title: '',),
                    )

                );
              },
            ),
            Divider(
              thickness: 1.0,
            ),


          ],
        ),
      ),

    );
  }
}

