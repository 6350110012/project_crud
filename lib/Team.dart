import 'dart:io';
import 'package:flutter/material.dart';


class ProfileDeveloper extends StatefulWidget {
  const ProfileDeveloper({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<ProfileDeveloper> createState() => _ProfileDeveloperState();
}

class _ProfileDeveloperState extends State<ProfileDeveloper> {



  @override


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.brown,
        title: Text('Team'),

      ),
      body: Container(
      child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
      Padding(
      padding: const EdgeInsets.only(top:40,bottom: 30),
      child: Image.asset("asset/images/Team.png", width: 200),
      ),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 25),
              child: CircleAvatar(
                backgroundImage: AssetImage("asset/images/green.jpg"),
                radius: 55,
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: Text("Nuchanard Viriyapuree",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w500,
                        color: Colors.brown,
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 5, left: 10),
                  child: Text("6350110012",
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                        color: Colors.brown,
                      )),
                ),
              ],
            ),
          ],
        ),

        Padding(
          padding: const EdgeInsets.only(top:10,bottom: 30),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 25,),
                child: CircleAvatar(
                  backgroundImage: AssetImage("asset/images/oat.jpg"),
                  radius: 55,
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Text("Suphakorn Sinsupparoek",
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w500,
                          color: Colors.brown,
                        )),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5, left: 10),
                    child: Text("6350110018",
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                          color: Colors.brown,
                        )),
                  ),
                ],
              ),
            ],
          ),
        ),
    ],
    ),
      ),
    );
  }
}
