import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class CreatePro extends StatefulWidget {
  @override
  _CreateProState createState() => _CreateProState();
}

class _CreateProState extends State<CreatePro> {
  // Handles text

  final nameController = TextEditingController();
  final priceController = TextEditingController();
  final descriptionController = TextEditingController();

  final GlobalKey<FormState> _key = new GlobalKey<FormState>();

  // Http post request to create new data
  Future _createProducts() async {
    return await http.post(
      Uri.parse("http://10.0.2.2/android/insert_product.php"),
      body: {
        "name": nameController.text,
        "price": priceController.text,
        "description": descriptionController.text,
      },
    );
  }

  void _onConfirm(context) async {
    await _createProducts();

    // Remove all existing routes until the Home.dart, then rebuild Home.
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Insert Bakery"),
        backgroundColor: Colors.brown,
      ),
      body: Container(
        height: 980.0,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top:50,bottom:20 ),
                child: Image.asset('asset/images/bakery.png',),
              ),



              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Container(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 40),
                          child: Form(
                            key: _key,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding:
                                  const EdgeInsets.only(right: 220, bottom: 5),
                                  child: Text("Bakery Name",
                                      style: TextStyle(
                                          fontSize: 15, color: Colors.black54)),
                                ),
                                TextFormField(
                                  textAlign: TextAlign.center,
                                  controller: nameController,
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Enter Bakery Name',
                                  ),
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Please Enter Name';
                                    }
                                    return null;
                                  },
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Padding(
                                  padding:
                                  const EdgeInsets.only(right: 220, bottom: 5),
                                  child: Text("Bakery Price",
                                      style: TextStyle(
                                          fontSize: 15, color: Colors.black54)),
                                ),
                                TextFormField(
                                  textAlign: TextAlign.center,
                                  controller: priceController,
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Enter Bakery Price',
                                  ),
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Please Enter Price';
                                    }

                                    return null;
                                  },
                                  keyboardType: TextInputType.number,
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Padding(
                                  padding:
                                  const EdgeInsets.only(right: 220, bottom: 5),
                                  child: Text("Description",
                                      style: TextStyle(
                                          fontSize: 15, color: Colors.black54)),
                                ),
                                TextFormField(
                                  textAlign: TextAlign.center,
                                  controller: descriptionController,
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    hintText: 'Enter Description',
                                  ),
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Please Enter description';
                                    }
                                    return null;
                                  },
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 30),
                                  child: ElevatedButton.icon(
                                    style: ButtonStyle(
                                        backgroundColor:
                                        MaterialStateProperty.all(Colors.brown)),
                                    onPressed: () {
                                      if (_key.currentState!.validate()) {
                                        _onConfirm(context);
                                      }
                                    },
                                    label: Text('SAVE',),
                                    icon: Icon(Icons.bakery_dining_outlined),

                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ]),
                ),
              ),
            ]),
      ),
    );

  }
}
