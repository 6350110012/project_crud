import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'Products.dart';
import 'edit.dart';

class Details extends StatefulWidget {
  final Products products;

  Details({required this.products});

  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  void deleteProducts(context) async {
    await http.post(
      Uri.parse("http://10.0.2.2/android/delete_product.php"),
      body: {
        'pid': widget.products.pid.toString(),
      },
    );
    // Navigator.pop(context);
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  void confirmDelete(context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Padding(
          padding: const EdgeInsets.only(),
          child: AlertDialog(
            content: Image.asset('asset/images/ขยะ.png',),


            actions: <Widget>[
              ElevatedButton.icon(
                label: Text('NO'),
                icon: Icon(Icons.cancel),
                style: ElevatedButton.styleFrom(
                  primary: Colors.red,
                  onPrimary: Colors.white,
                ),
                onPressed: () => Navigator.of(context).pop(),
              ),
              ElevatedButton.icon(
                label: Text('YES'),
                icon: Icon(Icons.check_circle),
                style: ElevatedButton.styleFrom(
                  primary: Colors.brown,
                  onPrimary: Colors.white,
                ),
                onPressed: () => deleteProducts(context),
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Bakery Details'),
        backgroundColor: Colors.brown,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.delete_outline),
            onPressed: () => confirmDelete(context),
          ),
        ],
      ),


      body:

      Padding(
          padding: const EdgeInsets.only(top: 20,left: 20),
          child: Container(
              child: Padding(
                padding: const EdgeInsets.only(top: 20,left: 25),
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[

                      Padding(
                        padding: const EdgeInsets.only(top: 20,bottom: 30,left:50),
                        child: CircleAvatar(backgroundImage:
                          AssetImage("asset/images/details.png",),
                              radius: 100
                          ),
                      ),

                      Text("Product Name : ${widget.products.name}",
                        style: TextStyle(fontSize: 21,color:Colors.brown ),
                      ),
                      SizedBox(height: 20,),
                      Text(
                        "Price : ${widget.products.price}",
                        style: TextStyle(fontSize: 21,color:Colors.brown ),
                      ),
                      SizedBox(height: 20,),
                      Text(
                        "Description : ${widget.products.description}",
                        style: TextStyle(fontSize: 21,color:Colors.brown ),
                      ),
                      SizedBox(height: 10,),
                    ],
                  ),
                ),
              ),


          )
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.edit),
        backgroundColor: Colors.brown,
        onPressed: () => Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => Edit(products: widget.products),
          ),
        ),
      ),
    );

  }
}


