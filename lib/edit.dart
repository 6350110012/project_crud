import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'Products.dart';

class Edit extends StatefulWidget {
  final Products products;

  Edit({required this.products});

  @override
  _EditState createState() => _EditState();
}

class _EditState extends State<Edit> {
  // This is for text to edit
  TextEditingController nameController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  final GlobalKey<FormState> _key=new GlobalKey<FormState>();

  // Http post request
  Future editProducts() async {
    return await http.post(
      Uri.parse("http://10.0.2.2/android/update_product.php"),
      body: {
        "pid": widget.products.pid.toString(),
        "name": nameController.text,
        "price": priceController.text,
        "description": descriptionController.text

      },
    );
  }

  void _onConfirm(context) async {
    await editProducts();
    // Remove all existing routes until the Home.dart, then rebuild Home.
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  @override
  void initState() {
    nameController = TextEditingController(text: widget.products.name);
    priceController = TextEditingController(text: widget.products.price.toString());
    descriptionController = TextEditingController(text: widget.products.description);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Bakery"),
        backgroundColor: Colors.brown,
      ),
      body: Container(

        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[



            Padding(
              padding: EdgeInsets.only(top:5,bottom:20,),
              child: Form(
                key: _key,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top:30,bottom:10 ),
                      child: SizedBox(width: 200,
                          child: Image.asset('asset/images/edit.png',)),
                    ),
                    SizedBox(height: 15,),
                    Padding(
                      padding:
                      const EdgeInsets.only(right: 300, bottom: 5),
                      child: Text("Bakery Price",
                          style: TextStyle(
                              fontSize: 15, color: Colors.black54)),
                    ),
                    TextFormField(
                      controller: nameController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Enter Bakery Name',
                      ),
                      validator: (value){
                        if(value==null || value.isEmpty){
                          return 'Please Enter Name';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 15,),
                    Padding(
                      padding:
                      const EdgeInsets.only(right: 300, bottom: 5),
                      child: Text("Bakery Price",
                          style: TextStyle(
                              fontSize: 15, color: Colors.black54)),
                    ),
                    TextFormField(
                      controller: priceController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Enter Bakery Price',
                      ),
                      validator: (value){
                        if(value==null || value.isEmpty){
                          return 'Please Enter Price';
                        }
                        return null;
                      },
                      keyboardType: TextInputType.number,
                    ),
                    SizedBox(height: 15,),
                    Padding(
                      padding:
                      const EdgeInsets.only(right: 300, bottom: 5),
                      child: Text("Bakery Price",
                          style: TextStyle(
                              fontSize: 15, color: Colors.black54)),
                    ),
                    TextFormField(
                      controller: descriptionController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Description',
                      ),
                      validator: (value){
                        if(value==null || value.isEmpty){
                          return 'Please Enter Description';
                        }
                        return null;
                      },
                    ),

                    Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: ElevatedButton.icon(
                          style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(Colors.brown)
                          ),
                          onPressed: (){
                            if(_key.currentState!.validate()) {
                              _onConfirm(context);
                            }
                          },
                        label: Text('SAVE',),
                        icon: Icon(Icons.bakery_dining_outlined),
                                ),
                    )
                  ],

                ),
              ),
            ),
          ],
        ),
      ),

    );


  }
}
